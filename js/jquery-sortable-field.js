(function ($) {

  /**
   * Sortable list's rows with field manipulation.
   */
  Drupal.behaviors.jquerySortableField = {
    attach: function (context, settings) {
      for (var base in settings.jquerySortableField) {
        $('#' + base, context).once('jquery-sortable-field', function () {
          // Create the new sortable instance. Save in the Drupal variable
          // to allow other scripts access to the object.
          Drupal.jquerySortableField[base] = new Drupal.jquerySortableField(this, settings.jquerySortableField[base]);
        });
      }
    }
  };

  /**
   * Constructor for the jquerySortableField object.
   *
   * @param list
   *   DOM object for the list to be made draggable.
   * @param listSettings
   *   Settings for the list.
   */
  Drupal.jquerySortableField = function (list, listSettings) {
    var self = this;

    // Required object variables.
    this.list = list;
    this.listSettings = listSettings;
    // Provides operations for row manipulation.
    this.rowObject = null;
    // Whether anything in the entire list has changed.
    this.changed = false;

    // Make each applicable row draggable.
    // Match immediate children of the parent element to allow nesting.
    $('> li', list).each(function () {
      self.makeDraggable(this);
    });

    // Add a link before the list for users to show or hide weight columns.
    $(list).before($('<a href="#" class="sortable-toggle-weight"></a>')
      .attr('title', Drupal.t('Re-order rows by numerical weight instead of dragging.'))
      .click(function (event) {
        if ($.cookie('Drupal.jquerySortableField.showWeight') == 1) {
          self.hideWeight();
        }
        else {
          self.showWeight();
        }

        event.preventDefault();
      })
      .wrap('<div class="sortable-toggle-weight-wrapper"></div>')
      .parent()
    );

    self.initWeights();

    $(list).sortable({
      handle: '.sortable-handle',
      placeholder: 'sortable-placeholder',
      containment: 'body',
      create: function (event, ui) {
        $(this).height($(this).height());
        // Call optional placeholder function.
        self.onCreate(event, ui.item, self);
      },
      start: function (event, ui) {
        ui.placeholder.height(ui.item.height());
        self.dragRow(event, ui.item, self);
        // Call optional placeholder function.
        self.onDrag(event, ui.item, self);
      },
      stop: function (event, ui) {
        self.unDragRow(event, ui.item, self);
        // Call optional placeholder function.
        self.onUnDrag(event, ui.item, self);
      },
      change: function (event, ui) {
        // Call optional placeholder function.
        self.onChange(event, ui.item, self);
      },
      update: function (event, ui) {
        self.dropRow(event, ui.item, self);
        // Call optional placeholder function.
        self.onDrop(event, ui.item, self);
      }
    });
  };

  /**
   * Initialize weight form elements to be hidden by default,
   * according to the settings for this jquerySortableField instance.
   *
   * Identify and mark each element with a CSS class so we can easily toggle
   * show/hide it. Finally, hide elements if user does not have a
   * 'Drupal.jquerySortableField.showWeight' cookie.
   */
  Drupal.jquerySortableField.prototype.initWeights = function () {
    $('> li', this.list).each(function () {
      $(this).find('div.delta-order').addClass('sortable-hide');
    });

    if ($.cookie('Drupal.jquerySortableField.showWeight') === null) {
      $.cookie('Drupal.jquerySortableField.showWeight', 0, {
        path: Drupal.settings.basePath,
        // The cookie expires in one year.
        expires: 365
      });
      this.hideWeight();
    }
    // Check cookie value and show/hide weight elements accordingly.
    else {
      if ($.cookie('Drupal.jquerySortableField.showWeight') == 1) {
        this.showWeight();
      }
      else {
        this.hideWeight();
      }
    }
  };

  /**
   * Hide the weight form elements.
   * Undo showWeight().
   */
  Drupal.jquerySortableField.prototype.hideWeight = function () {
    // Hide weight form elements.
    $('.sortable-hide', 'ul.jquery-sortable-field-processed').css('display', 'none');
    // Show jquerySortableField handles.
    $('.sortable-handle', 'ul.jquery-sortable-field-processed').css('display', '');

    // Change link text.
    $('.sortable-toggle-weight').text(Drupal.t('Show row weights'));
    // Change cookie.
    $.cookie('Drupal.jquerySortableField.showWeight', 0, {
      path: Drupal.settings.basePath,
      // The cookie expires in one year.
      expires: 365
    });
  };

  /**
   * Show the weight form elements.
   * Undo hideWeight().
   */
  Drupal.jquerySortableField.prototype.showWeight = function () {
    // Show weight form elements.
    $('.sortable-hide', 'ul.jquery-sortable-field-processed').css('display', '');
    // Hide jquerySortableField handles.
    $('.sortable-handle', 'ul.jquery-sortable-field-processed').css('display', 'none');

    // Change link text.
    $('.sortable-toggle-weight').text(Drupal.t('Hide row weights'));
    // Change cookie.
    $.cookie('Drupal.jquerySortableField.showWeight', 1, {
      path: Drupal.settings.basePath,
      // The cookie expires in one year.
      expires: 365
    });
  };

  /**
   * Find the target used within a particular row and group.
   */
  Drupal.jquerySortableField.prototype.rowSettings = function (group, row) {
    var $field = $('.' + group, row);
    for (var delta in this.listSettings[group]) {
      var targetClass = this.listSettings[group][delta].target;
      if ($field.is('.' + targetClass)) {
        // Return a copy of the row settings.
        var rowSettings = {};
        for (var n in this.listSettings[group][delta]) {
          rowSettings[n] = this.listSettings[group][delta][n];
        }
        return rowSettings;
      }
    }
  };

  /**
   * Take an item draggable.
   */
  Drupal.jquerySortableField.prototype.makeDraggable = function (item) {
    // Create the handle.
    var handle = $('<span class="sortable-handle"><span class="handle">&nbsp;</span></span>').attr('title', Drupal.t('Drag to re-order'));
    $(item).prepend(handle);

    // Add hover action for the handle.
    handle.hover(function () {
      $(this).addClass('sortable-handle-hover');
    }, function () {
      $(this).removeClass('sortable-handle-hover');
    });
  };

  /**
   * Drag row action.
   */
  Drupal.jquerySortableField.prototype.dragRow = function (event, item, self) {
    // Create a new rowObject for manipulation of this row.
    this.rowObject = new this.row(item);

    item.addClass('drag').find('span.sortable-handle').addClass('sortable-handle-hover');
  };

  /**
   * Un-drag row action.
   */
  Drupal.jquerySortableField.prototype.unDragRow = function (event, item, self) {
    item.removeClass('drag');
  };

  /**
   * Drop row action
   */
  Drupal.jquerySortableField.prototype.dropRow = function (event, item, self) {
    if (self.rowObject != null) {
      var $droppedRow = self.rowObject.element;

      // Update the fields in the dropped row.
      self.updateFields($droppedRow);

      self.rowObject.markChanged();
      if (self.changed == false) {
        $(Drupal.theme('jquerySortableFieldChangedWarning')).insertBefore(self.list).hide().fadeIn('slow');
        self.changed = true;
      }

      $droppedRow.removeClass('drag').addClass('drag-previous');
      self.rowObject = null;
    }
  };

  /**
   * After the row is dropped, update the list's fields according to the settings
   * set for this list.
   */
  Drupal.jquerySortableField.prototype.updateFields = function (changedRow) {
    for (var group in this.listSettings) {
      this.updateField(changedRow, group);
    }
  };

  /**
   * After the row is dropped, update a single list's field according to specific
   * settings.
   */
  Drupal.jquerySortableField.prototype.updateField = function (changedRow, group) {

    // Siblings are easy, check previous and next rows.
    var previousRow = changedRow.prev('li');
    var nextRow = changedRow.next('li');
    var sourceRow = changedRow;
    if ($(previousRow).is('.draggable') && $('.' + group, previousRow).length) {
      sourceRow = previousRow;
    }
    else if ($(nextRow).is('.draggable') && $('.' + group, nextRow).length) {
      sourceRow = nextRow;
    }

    this.copyDragClasses(sourceRow, changedRow, group);
    var rowSettings = this.rowSettings(group, changedRow);

    var targetClass = '.' + rowSettings.target;
    var targetElement = $(targetClass, changedRow).get(0);

    // Check if a target element exists in this row.
    if (targetElement) {
      var siblings = this.rowObject.findSiblings(rowSettings);
      if ($(targetElement).is('select')) {
        // Get a list of acceptable values.
        var values = [];
        $('option', targetElement).each(function () {
          values.push(this.value);
        });

        var maxVal = values[values.length - 1];
        // Populate the values in the siblings.
        $(targetClass, siblings).each(function () {
          // If there are more items than possible values, assign the maximum value to the row.
          if (values.length > 0) {
            this.value = values.shift();
          }
          else {
            this.value = maxVal;
          }
        });
      }
      else {
        // Assume a numeric input field.
        var weight = parseInt($(targetClass, siblings[0]).val(), 10) || 0;
        $(targetClass, siblings).each(function () {
          this.value = weight;
          weight++;
        });
      }
    }
  };

  /**
   * Copy all special jquerySortableField classes from one row's form elements to a
   * different one, removing any special classes that the destination row
   * may have had.
   */
  Drupal.jquerySortableField.prototype.copyDragClasses = function (sourceRow, targetRow, group) {
    var sourceElement = $('.' + group, sourceRow);
    var targetElement = $('.' + group, targetRow);
    if (sourceElement.length && targetElement.length) {
      targetElement[0].className = sourceElement[0].className;
    }
  };

  /**
   * Constructor to make a new object to manipulate a list item.
   */
  Drupal.jquerySortableField.prototype.row = function (item) {
    this.element = item;
    this.changed = false;
    this.list = item.closest('ul').get(0);
  };

  /**
   * Find all siblings for a row.
   */
  Drupal.jquerySortableField.prototype.row.prototype.findSiblings = function (rowSettings) {
    var siblings = [];
    var directions = ['prev', 'next'];
    for (var d = 0; d < directions.length; d++) {
      var checkRow = this.element[directions[d]]();
      while (checkRow.length) {
        // Check that the sibling contains a similar target field.
        if ($('.' + rowSettings.target, checkRow)) {
          siblings.push(checkRow[0]);
        }
        else {
          break;
        }
        checkRow = $(checkRow)[directions[d]]();
      }
      // Since siblings are added in reverse order for previous, reverse the
      // completed list of previous siblings. Add the current row and continue.
      if (directions[d] == 'prev') {
        siblings.reverse();
        siblings.push(this.element[0]);
      }
    }
    return siblings;
  };

  /**
   * Add an asterisk or other marker to the changed row.
   */
  Drupal.jquerySortableField.prototype.row.prototype.markChanged = function () {
    var marker = Drupal.theme('jquerySortableFieldChangedMarker');
    if ($('span.sortable-changed', this.element).length == 0) {
      this.element.find('span.sortable-handle').prepend(marker);
    }
  };

  /**
   * Stub function. Allows a custom handler when a initialize the sortable.
   */
  Drupal.jquerySortableField.prototype.onCreate = function (event, item, self) {
    return null;
  };

  /**
   * Stub function. Allows a custom handler when a row begins dragging.
   */
  Drupal.jquerySortableField.prototype.onDrag = function (event, item, self) {
    return null;
  };

  /**
   * Stub function. Allows a custom handler when a row dropped in old position.
   */
  Drupal.jquerySortableField.prototype.onUnDrag = function (event, item, self) {
    return null;
  };

  /**
   * Stub function. Allows a custom handler when a row is changed position.
   */
  Drupal.jquerySortableField.prototype.onChange = function (event, item, self) {
    return null;
  };

  /**
   * Stub function. Allows a custom handler when a row is dropped.
   */
  Drupal.jquerySortableField.prototype.onDrop = function (event, item, self) {
    return null;
  };

  Drupal.theme.prototype.jquerySortableFieldChangedMarker = function () {
    return '<span class="warning sortable-changed">*</span>';
  };

  Drupal.theme.prototype.jquerySortableFieldChangedWarning = function () {
    return '<div class="sortable-changed-warning messages warning">' + Drupal.theme('jquerySortableFieldChangedMarker') + ' ' + Drupal.t('Changes made in this field will not be saved until the form is submitted.') + '</div>';
  };

})(jQuery);
